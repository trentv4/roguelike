package com.trentv4.game.entity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;

import com.trentv4.game.content.GameLogger;
import com.trentv4.game.content.GameLoop;
import com.trentv4.game.content.Position;
import com.trentv4.game.core.MainGame;
import com.trentv4.game.terrain.Terrain;

public class Entity implements Cloneable
{
	public Position pos;
	public EntityStatistics stats;
	public String program;
	public int UUID;
	
	public String className;

	private static int MAX_UUID = 1;
	private static HashMap<String, Entity> ENT_LIST = new HashMap<String, Entity>();
	private static final String[] DEATH_MESSAGES = {
		" collapses in a pool of blood!",
		" dies!",
		" keels over in pain!",
		" suffered a terrible end!",
		" bit the dust!" };
	
	//Constructors
	public Entity()
	{
		this.pos = new Position();
		this.stats = new EntityStatistics();
		this.program = "";
		this.className = "Entity";
		this.UUID = MAX_UUID;
		MAX_UUID++;
	}

	//Static helper methods
	public static final void populate()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader(new File("entities.txt")));
			String next = reader.readLine();
			while(next != null)
			{
				Entity a = GameLoop.gson.fromJson(next, Entity.class);
				if(!a.className.equals("Entity"))
				{
					a = (Entity) GameLoop.gson.fromJson(next, Class.forName(a.className));
				}
				ENT_LIST.put(a.stats.ENT_ID, a);
				next = reader.readLine();
			}
			reader.close();
			
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static final Entity getNewEntity(String lookup)
	{
		try
		{
			if(lookup.equals("RANDOM"))
			{
				Entity[] collection = (Entity[]) ENT_LIST.values().toArray();
				Entity a = collection[(int) Math.floor(Math.random() * collection.length)];
				if(a != null)
				{
					return a;
				}
			}
			else
			{
				Entity a = (Entity) ENT_LIST.get(lookup).clone();
				if(a != null)
				{
					a.UUID = MAX_UUID;
					MAX_UUID++;
					return a;
				}
			}
			MainGame.crash(new Exception("Unable to look up entity: " + lookup));

		} catch (Exception e)
		{
			MainGame.crash(e);
		}
		return null;
	}
	
	//Nonstatic helper methods
	public final void kill()
	{
		GameLogger.log(DEATH_MESSAGES[(int) (Math.floor(Math.random()*DEATH_MESSAGES.length))]);
		GameLoop.removeEntity(this);
	}

	public final boolean canTick(int turnCycle)
	{
		if(turnCycle <= stats.speed) return true;
		return false;
	}

	//Subclassing methods
	/** Fires every tick the entity is allowed to according to it's speed. */
	public void tick()
	{
		AIProgram.get(program).invoke(this);
	}
	
	/** Attempts to move the entity by a delta of x,y,z. Checks for terrain and moves into it if able, and fires Terrain.onWalkInto */
	public void move(int x, int y, int z)
	{
		Terrain t = GameLoop.getMap().getTerrain(pos.x + x, pos.y + y, pos.z + z);
		t.onWalkInto(this);
		if(t.stats.isSolid)
		{
			this.pos.x = pos.x + x;
			this.pos.y = pos.x + y;
			this.pos.z = pos.x + z;
		}
	}
}