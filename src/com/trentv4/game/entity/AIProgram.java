package com.trentv4.game.entity;

public class AIProgram
{
	public void invoke(Entity invoker) {}
	
	private static AIProgramBasic basic = new AIProgramBasic();
	private static AIProgram none = new AIProgram();
	
	public static AIProgram get(String a)
	{
		switch(a)
		{
		case "NONE": return none;
		case "BASIC": return basic;
		}
		return null;
	}
}
