package com.trentv4.game.entity;

public class EntityStatistics
{
	public int speed;
	
	public int health;
	public int maxHealth;
	
	public int armorClass;
	public int hitCount; 
	public int hitDice;  //hitCount d hitDice + hitBonus
	public int hitBonus; //ex. 2d6+1
	
	public String name;
	
	public String ENT_ID;
	public String[] FRIENDS_ID;
	
	public EntityStatistics()
	{
		this.speed = 0;
		this.health = 1;
		this.maxHealth = 1;
		this.armorClass = 0;
		this.hitBonus = 0;
		this.hitCount = 0;
		this.hitDice = 0;
		this.name = "UNKNOWN ENTITY";
		this.ENT_ID = "UNKNOWN";
		this.FRIENDS_ID = new String[]{};
	}
	
	public EntityStatistics(int health, int maxHealth, int armorClass, int hitCount,
			int hitDice, int hitBonus, String name, String ENT_ID, String[] FRIENDS_ID)
	{
		this.health = health;
		this.maxHealth = maxHealth;
		this.armorClass = armorClass;
		this.hitCount = hitCount;
		this.hitDice = hitDice;
		this.hitBonus = hitBonus;
		this.name = name;
		this.ENT_ID = ENT_ID;
		this.FRIENDS_ID = FRIENDS_ID;
	}
}