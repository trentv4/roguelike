package com.trentv4.game.content;

public class Position
{
	public int x;
	public int y;
	public int z;
	
	public Position()
	{
		this.x = 0;
		this.y = 0;
		this.z = 0;
	}
	
	public Position(int x, int y, int z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
}
