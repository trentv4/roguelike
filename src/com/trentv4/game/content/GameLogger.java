package com.trentv4.game.content;

import java.util.ArrayList;

public final class GameLogger
{
	private static ArrayList<String> logMessages = new ArrayList<String>();
	public static int logOffset = 0;

	/** Private constructor so this class cannot be instantiated. */
	private GameLogger()
	{
	};

	/**
	 * Prints a log message with a where <i>message</i> will be designated
	 * <i>Important</i> level.
	 */
	public static final void log(Object message)
	{
		String messagec = message.toString();
		logMessages.add(messagec);
		logOffset = 0;
	}

	public static final String get(int id)
	{
		if (id < 0)
		{
			id = -id;
			int pos = getSize() - (id + logOffset);
			if (pos >= 0 && pos < getSize())
			{
				return logMessages.get(pos);
			}
		}
		if (id + logOffset >= 0)
		{
			if (id + logOffset < getSize())
			{
				return logMessages.get(id + logOffset);
			}
		}
		return "";
	}

	public static final int getSize()
	{
		return logMessages.size();
	}
}
