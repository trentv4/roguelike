package com.trentv4.game.content;

import java.util.ArrayList;

import com.trentv4.game.entity.Entity;
import com.trentv4.game.items.Item;
import com.trentv4.game.terrain.Terrain;

public class GameMap
{
	public Terrain[][][] terrain = new Terrain[25][25][4];
	public Item[][][] items = new Item[25][25][4];
	public ArrayList<Entity> entities = new ArrayList<Entity>();

	public GameMap() {}
	
	public void placeEntityRandomly(Entity a)
	{
		boolean unplaced = true;
		while(unplaced)
		{
			int x = (int) Math.floor(Math.random() * 25);
			int y = (int) Math.floor(Math.random() * 25);
			int z = (int) Math.floor(Math.random() * 4);
			
			if(getTerrain(x,y,z).equals("NONE"))
			{
				Entity[] b = getEntities();
				boolean entCheck = false;
				for(int i = 0; i < b.length; i++)
				{
					if(b[i].pos.x == x)
					{
						if(b[i].pos.y == y)
						{
							if(b[i].pos.z == z)
							{
								entCheck = true;
							}
						}
					}
				}
				if(!entCheck)
				{
					entities.add(a);
					a.pos.x = x;
					a.pos.y = y;
					a.pos.z = z;
					unplaced = false;
				}
			}
		}
	}
	
	public Entity[] getEntities()
	{
		return entities.toArray(new Entity[entities.size()]);
	}
	
	public Terrain getTerrain(int x, int y, int z)
	{
		return terrain[x][y][z];
	}
	
	public void setTerrain(Terrain t, int x, int y, int z)
	{
		terrain[x][y][z] = t;
	}
	
	public Item getItems(int x, int y, int z)
	{
		return items[x][y][z];
	}
	
	public void setItems(Item i, int x, int y, int z)
	{
		items[x][y][z] = i;
	}
}
