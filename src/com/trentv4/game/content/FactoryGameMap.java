package com.trentv4.game.content;

import com.trentv4.game.terrain.Terrain;

public class FactoryGameMap
{
	public static GameMap generateMap(int x, int y)
	{
		GameMap a = new GameMap();
		return a;
	}
	
	public static GameMap generateMap()
	{
		GameMap a = new GameMap();
		a = genCavesBasic(a);
		return a;
	}
	
	public static GameMap genCavesBasic(GameMap map)
	{
		for(int x = 0; x < 25; x++)
		{
			for(int y = 0; y < 25; y++)
			{
				for(int z = 0; z < 4; z++)
				{
					Terrain t = Terrain.getNewTerrain("T_STONE");
					t.pos = new Position(x,y,z);
					map.setTerrain(t, x, y, z);
				}
			}
		}
		return map;
	}
}