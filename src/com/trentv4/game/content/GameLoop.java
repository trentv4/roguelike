package com.trentv4.game.content;

import java.util.ArrayList;
import java.util.Random;

import com.google.gson.Gson;
import com.trentv4.game.core.AudioHandler;
import com.trentv4.game.core.DisplayManager;
import com.trentv4.game.core.InputMapper;
import com.trentv4.game.core.Texture;
import com.trentv4.game.entity.Entity;
import com.trentv4.game.items.Item;
import com.trentv4.game.liquid.Liquid;
import com.trentv4.game.logging.LogLevel;
import com.trentv4.game.logging.Logger;
import com.trentv4.game.renderer.Renderer;
import com.trentv4.game.renderer.RendererMainGame;
import com.trentv4.game.terrain.Terrain;

/**
 * Primary loop class for the program. This class only exposes one method:
 * <i>run()</i>, which will run differing methods depending on program state.
 */
public final class GameLoop
{
	public static Entity player;
	public static ArrayList<GameMap> maps = new ArrayList<GameMap>();
	public static int mapIndex = 0;
	public static Gson gson = new Gson();
	public static int[] camera = { 0, 0 };
	public static Renderer renderer;

	private static int turnCycle = 0;
	private static boolean isWaiting = false;
	private static boolean isInitialized;
	private static final Random random = new Random();

	public static final void run()
	{
		if (!isInitialized())
		{
			Texture.setLoadAlerts(false);
			AudioHandler.initialize();
			initialize();
			isInitialized = true;
		}
		if (DisplayManager.isInitialized())
		{
			runIngame();
			InputMapper.tick();
			InputMapper.update();
		}
	}

	private static final void runIngame()
	{
		if(!isWaiting)
		{
			Logger.log(LogLevel.NOTE, "Tick processed: " + turnCycle);
			Entity[] e = getMap().getEntities();
			for(int i = 0; i < e.length; i++)
			{
				if(e[i].canTick(turnCycle)) e[i].tick();
			}
			if(turnCycle == 4)
			{
				turnCycle = 0;
			}
			else
			{
				turnCycle++;
			}
		}
		if(player.canTick(turnCycle))
		{
			isWaiting = true;
		}
	}

	public static final void removeEntity(Entity e)
	{
		for (int i = 0; i < getMap().entities.size(); i++)
		{
			if (e.UUID == getMap().entities.get(i).UUID)
			{
				getMap().entities.remove(i);
				return;
			}
		}
	}

	public static GameMap getMap()
	{
		return maps.get(mapIndex);
	}

	public static final void addEntity(Entity e)
	{
		getMap().entities.add(e);
	}

	public static Random getRandom()
	{
		return random;
	}

	//////
	
	public static boolean isInitialized()
	{
		return isInitialized;
	}

	public static final void initialize()
	{
		long seed = (long) Math.floor(Math.random() * 2000000000);
		random.setSeed(seed);
		Entity.populate();
		Terrain.populate();
		Item.populate();
		Liquid.populate();
		
		renderer = new RendererMainGame();
		GameLogger.log("Game loaded.");
		maps.add(FactoryGameMap.generateMap());
		player = Entity.getNewEntity("E_PLAYER");
		camera = new int[]{player.pos.x, player.pos.y};
//		getMap().placeEntityRandomly(player);
	}

	public static final void draw()
	{
		if(renderer != null)
		{
			renderer.render();
		}
	}
}
