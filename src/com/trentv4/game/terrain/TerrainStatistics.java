package com.trentv4.game.terrain;

public class TerrainStatistics
{
	public String name;
	public boolean isSolid;
	
	public String TER_ID;
	
	public TerrainStatistics()
	{
		this.name = "UNKNOWN TERRAIN";
		this.isSolid = true;
		this.TER_ID = "UNKNOWN";
	}
}
