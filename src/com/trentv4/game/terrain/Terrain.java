package com.trentv4.game.terrain;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;

import com.trentv4.game.content.GameLoop;
import com.trentv4.game.content.Position;
import com.trentv4.game.core.MainGame;
import com.trentv4.game.entity.Entity;

public class Terrain implements Cloneable
{
	public Position pos;
	public TerrainStatistics stats;
	
	public String className;
	
	private static HashMap<String, Terrain> TER_LIST = new HashMap<String, Terrain>();

	//Constructors
	public Terrain()
	{
		this.pos = new Position();
		this.stats = new TerrainStatistics();
		this.className = "Terrain";
	}
	
	//Static helper methods
	public static final void populate()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader(new File("terrain.txt")));
			String next = reader.readLine();
			while(next != null)
			{
				Terrain a = GameLoop.gson.fromJson(next, Terrain.class);
				if(!a.className.equals("Terrain"))
				{
					a = (Terrain) GameLoop.gson.fromJson(next, Class.forName(a.className));
				}
				TER_LIST.put(a.stats.TER_ID, a);
				next = reader.readLine();
			}
			reader.close();
			
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static final Terrain getNewTerrain(String lookup)
	{
		try
		{
			if(lookup.equals("RANDOM"))
			{
				Object[] collection = TER_LIST.values().toArray();
				Terrain a = (Terrain) collection[(int) Math.floor(Math.random() * collection.length)];
				if(a != null)
				{
					return a;
				}
			}
			else
			{
				Terrain a = (Terrain) TER_LIST.get(lookup).clone();
				if(a != null)
				{
					return a;
				}
			}
			MainGame.crash(new Exception("Unable to look up terrain: " + lookup));
			return null;

		} catch (Exception e)
		{
			MainGame.crash(e);
			return null;
		}
	}
	
	//Subclassing methods
	/** Fires every single tick, regardless of which speed tick it is. */
	public void tick()
	{

	}

	/** Fires when an entity walks into it. */
	public void onWalkInto(Entity invoker)
	{
		
	}
}
