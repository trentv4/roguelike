package com.trentv4.game.terrain.subtypes;

import com.trentv4.game.entity.Entity;
import com.trentv4.game.terrain.Terrain;

public class TerrainDoor extends Terrain
{
	public boolean isOpen = true;
	
	public TerrainDoor()
	{
		super();
		this.isOpen = true;
		this.stats.isSolid = true;
	}
	
	@Override
	public void onWalkInto(Entity invoker)
	{
		this.isOpen = !isOpen;
		this.stats.isSolid = isOpen;
	}
}
