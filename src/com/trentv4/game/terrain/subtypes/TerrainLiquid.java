package com.trentv4.game.terrain.subtypes;

import com.trentv4.game.liquid.Liquid;
import com.trentv4.game.terrain.Terrain;

public class TerrainLiquid extends Terrain
{
	public Liquid liquid;
	private String liquidLoader = "";
	
	public TerrainLiquid()
	{
		super();
		this.liquidLoader = "";
	}
	
	@Override
	public void tick()
	{
		if(liquid == null) liquid = Liquid.getNewLiquid(liquidLoader);
	}
}
