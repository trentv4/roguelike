package com.trentv4.game.terrain.subtypes;

import com.trentv4.game.entity.Entity;
import com.trentv4.game.terrain.Terrain;

public class TerrainLadder extends Terrain
{
	public boolean isDown;
	
	public TerrainLadder()
	{
		super();
		this.isDown = true;
	}
	
	@Override
	public void onWalkInto(Entity invoker)
	{
		
	}
}
