package com.trentv4.game.liquid;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;

import com.trentv4.game.content.GameLoop;
import com.trentv4.game.core.MainGame;

public class Liquid
{
	public String className;
	public LiquidStatistics stats;
	
	private static HashMap<String, Liquid> LQD_LIST = new HashMap<String, Liquid>();
	
	//Constructors
	public Liquid()
	{
		this.className = "Liquid";
		this.stats = new LiquidStatistics();
	}
	
	//Static helper methods
	public static final void populate()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader(new File("liquids.txt")));
			String next = reader.readLine();
			while(next != null)
			{
				Liquid a = GameLoop.gson.fromJson(next, Liquid.class);
				if(!a.className.equals("Liquid"))
				{
					a = (Liquid) GameLoop.gson.fromJson(next, Class.forName(a.className));
				}
				LQD_LIST.put(a.stats.LQD_ID, a);
				next = reader.readLine();
			}
			reader.close();
			
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static final Liquid getNewLiquid(String lookup)
	{
		try
		{
			if(lookup.equals("RANDOM"))
			{
				Object[] collection = LQD_LIST.values().toArray();
				Liquid a = (Liquid) collection[(int) Math.floor(Math.random() * collection.length)];
				if(a != null)
				{
					return a;
				}
			}
			else
			{
				Liquid a = (Liquid) LQD_LIST.get(lookup).clone();
				if(a != null)
				{
					return a;
				}
			}
			MainGame.crash(new Exception("Unable to look up liquid: " + lookup));
			return null;

		} catch (Exception e)
		{
			MainGame.crash(e);
			return null;
		}
	}
	
	//Subclassing methods
	/** Fires every single tick, regardless of which speed tick it is. */
	public void onTick()
	{
		
	}
}