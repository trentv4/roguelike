package com.trentv4.game.core;

import java.io.File;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;

import com.trentv4.game.logging.LogLevel;
import com.trentv4.game.logging.Logger;

public final class AudioHandler
{
	private static Clip musicClip;

	public static void initialize()
	{
		Logger.log(LogLevel.INIT_NOTE, "Initializing audio handler...");
		try
		{
			musicClip = AudioSystem.getClip();
		} catch (LineUnavailableException e)
		{
			Logger.log("Unable to get clip from AudioSystem! Crashing...");
			MainGame.crash(e);
		}
		Logger.log(LogLevel.INIT_NOTE, "Audio handler initialized!");
	}

	public static final void setMusic(String path)
	{
		try
		{
			musicClip.stop();
			AudioInputStream audioIn = AudioSystem.getAudioInputStream(new File("sounds/" + path));
			musicClip = AudioSystem.getClip();
			musicClip.open(audioIn);
			musicClip.start();
		} catch (Exception e)
		{
			Logger.log(LogLevel.ERROR, "Unable to set music: " + path);
		}
	}

	public static final void playSound(String path)
	{
		try
		{
			AudioInputStream audioIn = AudioSystem.getAudioInputStream(new File("sounds/" + path));
			Clip clip = AudioSystem.getClip();
			clip.open(audioIn);
			clip.start();
		} catch (Exception e)
		{
			Logger.log(LogLevel.ERROR, "Unable to play sound: " + path);
		}
	}
}
