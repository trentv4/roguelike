package com.trentv4.game.core;

import java.io.File;

import com.trentv4.game.content.GameLoop;
import com.trentv4.game.logging.LogLevel;
import com.trentv4.game.logging.Logger;
import com.trentv4.game.properties.Properties;

/**
 * Main class of the program. This handles the life status of the program, all
 * tracked threads, stores the program directory, and provides several useful
 * methods in the the general execution of the program. These methods include:
 * <ul>
 * <li>void <i>kill()</i>, which is used to universally end the program,</li>
 * <li>void <i>crash(Exception)</i>, which is used to print an error log and
 * then kill the program,</li>
 * <li>boolean <i>isAlive()</i>, which is used to check if the program is still
 * alive,</li>
 * <li>String <i>getPath()</i>, which is used to retreive the current folder
 * path.</li>
 * </ul>
 * This class also contains a private Thread SystemLoop, which runs
 * <i>GameLoop.run()</i> at maximum speed.
 */
public class MainGame
{
	private static boolean isRunning = false;
	private static MainGame theGame;
	private static String path;
	public static String version = "0.5 Pre-Alpha";
	private static int lastSecondFPS = 0;
	private static final int TICK_RATE = 32;

	/** Main entry point of the program. Accepts no arguments. */
	public static void main(String[] args) // will never be thrown
	{
		Logger.initialize(LogLevel.INIT_NOTE);
		try
		{
			theGame = new MainGame();
			setPath(new File("").getCanonicalPath() + "/"); // fix me.
			Logger.log(LogLevel.INIT_NOTE, "Created path at " + path);
			Texture.init();
			Logger.log(LogLevel.INIT_NOTE, "Initialized ImageHandler");
			isRunning = true;
			Logger.log(LogLevel.INIT_NOTE, "Game is now running!");
			Thread t = new Thread(theGame.new SystemLoop());
			t.setName("Roguelike game loop");
			t.start();
			Logger.log(LogLevel.INIT_NOTE, "SystemLoop thread started.");
			DisplayManager.initialize(new Properties());
		} catch (Exception e)
		{
			crash(e);
		}
	}

	/** Kills the program. */
	public static void kill()
	{
		Logger.log("Program is being killed.");
		isRunning = false;
		Logger.saveLog("log.txt");
		System.exit(0); // I ain't havin' no leakin' threads on my watch!
	}

	/** Prints the Exception and then kills the program. */
	public static final void crash(Exception e)
	{
		Logger.log("Program has crashed! Printing error log: ");
		e.printStackTrace();
		kill(); // RIP
	}

	/**
	 * Checks the status of the program (if executing or not). This allows for
	 * safe, <i>ConcurrentModificationException</i>-free heartbeat checking.
	 */
	public static boolean isAlive()
	{
		return isRunning;
	}

	/** Gets the FPS from the last second for the game loop. */
	public static int getGameLoopFPS()
	{
		return lastSecondFPS;
	}

	/**
	 * Returns the current directory, ready-formatted for use, e.g.
	 * "<i>/home/trent/execution_folder/</i>".
	 */
	public static String getPath()
	{
		return path;
	}

	private static void setPath(String path)
	{
		MainGame.path = path;
	}

	private class SystemLoop implements Runnable
	{
		@Override
		public void run()
		{
			long resolution = 1000000000 / TICK_RATE;
			long startTime = System.nanoTime();
			long currentTime = startTime;
			while (isRunning)
			{
				currentTime = System.nanoTime();
				if (currentTime - startTime > resolution)
				{
					startTime = System.nanoTime();
					currentTime = startTime;
					GameLoop.run();
				}
			}
		}
	}
}
