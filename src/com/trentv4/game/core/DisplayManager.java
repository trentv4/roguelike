package com.trentv4.game.core;

import static org.lwjgl.glfw.Callbacks.errorCallbackPrint;
import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;
import static org.lwjgl.glfw.GLFW.GLFW_RESIZABLE;
import static org.lwjgl.glfw.GLFW.GLFW_VISIBLE;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSetCursorPosCallback;
import static org.lwjgl.glfw.GLFW.glfwSetErrorCallback;
import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetMouseButtonCallback;
import static org.lwjgl.glfw.GLFW.glfwSetScrollCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowSizeCallback;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TRIANGLE_STRIP;
import static org.lwjgl.opengl.GL11.GL_TRUE;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glTranslated;
import static org.lwjgl.opengl.GL11.glVertex2d;
import static org.lwjgl.system.MemoryUtil.NULL;

import java.awt.Color;

import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWScrollCallback;
import org.lwjgl.glfw.GLFWWindowSizeCallback;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLContext;

import com.trentv4.game.content.GameLoop;
import com.trentv4.game.logging.LogLevel;
import com.trentv4.game.logging.Logger;
import com.trentv4.game.properties.Properties;

/**
 * Principal class that handles the display. This creates a glfw window and uses
 * OpenGL to draw. This window will call <i>MainGame.kill()</i> on exit. This
 * class provides several methods for manipulating the frame:
 * <ul>
 * <li>Properties <i>getProperties()</i>, which returns the current Properties
 * object.</li>
 * <li>boolean <i>isInitialized()</i>, which returns if the manager is
 * initialized yet.</li>
 * </ul>
 * In addition, this class has an <i>inititialize()</i> method which is called
 * from <i>MainGame</i>. This method is NOT safe to call, and will generally
 * fuck up everything if you do. Don't!
 */
public class DisplayManager
{
	private static GLFWErrorCallback errorCallback;
	private static GLFWKeyCallback keyCallback;
	@SuppressWarnings("unused")
	// whyy
	private static GLFWScrollCallback scrollCallback;
	private static GLFWMouseButtonCallback mouseButtonCallback;
	private static GLFWCursorPosCallback mousePosCallback;
	@SuppressWarnings("unused")
	// this has no reason to be here.
	private static GLFWWindowSizeCallback resizeCallback;
	private static long glfwWindowContext;
	private static Properties properties;
	private static boolean isInitialized = false;
	private static boolean waitingForViewport = false;

	public static final void initialize(Properties properties)
	{
		Thread.currentThread().setName("Roguelike display loop");
		Logger.log(LogLevel.INIT_NOTE, "Initializing DisplayManager..");
		DisplayManager.properties = properties;
		// Set up error logging
		glfwSetErrorCallback(errorCallback = errorCallbackPrint(System.err));

		// Create glfw window
		if (glfwInit() != GL11.GL_TRUE)
		{
			MainGame.crash(new RuntimeException("Unable to initialize glfw window!"));
			return;
		}

		// Config window
		glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
		glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
		glfwWindowContext = glfwCreateWindow(properties.getWidth(), properties.getHeight(), properties.getTitle(), NULL, NULL);
		if (glfwWindowContext == NULL)
		{
			MainGame.crash(new RuntimeException("Unable to initialize glfw window!"));
			return;
		}

		glfwMakeContextCurrent(glfwWindowContext);
		// Vsync
		// glfwSwapInterval(0);

		// Key Input

		glfwSetKeyCallback(glfwWindowContext, keyCallback = new GLFWKeyCallback()
		{
			@Override
			public void invoke(long window, int key, int scancode, int action, int mods)
			{
				if (action == GLFW_PRESS)
					InputMapper.setStatus(key, true);
				if (action == GLFW_RELEASE)
					InputMapper.setStatus(key, false);
			}
		});

		// Mouse Input

		glfwSetMouseButtonCallback(glfwWindowContext, mouseButtonCallback = new GLFWMouseButtonCallback()
		{
			@Override
			public void invoke(long window, int button, int action, int mods)
			{
				if (action == 0)
					InputMapper.setMouseUp(button);
				if (action == 1)
					InputMapper.setMouseDown(button);
			}
		});

		glfwSetCursorPosCallback(glfwWindowContext, mousePosCallback = new GLFWCursorPosCallback()
		{
			@Override
			public void invoke(long window, double x, double y)
			{
				InputMapper.setMousePos((int) x, (int) y);
			}
		});

		// resize callback
		glfwSetWindowSizeCallback(glfwWindowContext, resizeCallback = new GLFWWindowSizeCallback()
		{
			@Override
			public void invoke(long window, int x, int y)
			{
				DisplayManager.properties.setSize(x, y);
				waitingForViewport = true;
			}
		});

		// scroll callback
		glfwSetScrollCallback(glfwWindowContext, scrollCallback = new GLFWScrollCallback()
		{
			@Override
			public void invoke(long window, double xoffset, double yoffset)
			{
				InputMapper.setMouseWheelDelta((int) yoffset);
			}
		});

		// Show!
		glfwShowWindow(glfwWindowContext);

		// yaaay

		// loop
		GLContext.createFromCurrent();

		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glOrtho(0, properties.getWidth(), properties.getHeight(), 0, 1, -1);
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);

		isInitialized = true;
		Logger.log(LogLevel.INIT_NOTE, "DisplayManager initialized!");
		while (MainGame.isAlive())
		{
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			if (waitingForViewport)
			{
				GL11.glViewport(0, 0, DisplayManager.properties.getWidth(), DisplayManager.properties.getHeight());
			}
			if (GameLoop.isInitialized())
			{
				GameLoop.draw();
			}
			glfwSwapBuffers(glfwWindowContext);
			glfwPollEvents();
			if (glfwWindowShouldClose(glfwWindowContext) == GL_TRUE)
				MainGame.kill();
		}
		exit();
	}

	private static final void exit()
	{
		glfwTerminate();
		keyCallback.release();
		errorCallback.release();
		mouseButtonCallback.release();
		mousePosCallback.release();
	}

	public static Properties getProperties()
	{
		return properties;
	}

	public static boolean isInitialized()
	{
		return isInitialized;
	}

	public static final void drawImage(String id, double x, double y, double xSize, double ySize)
	{
		Texture texture = Texture.loadTexture(id);
		glPushMatrix();
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, texture.id);
		glColor4f(127.5F, 127.5F, 127.5F, 1);
		glTranslated(x, y - ySize, 0);
		glBegin(GL_TRIANGLE_STRIP);
		glTexCoord2f(0, 0);
		glVertex2d(0, 0);
		glTexCoord2f(1, 0);
		glVertex2d(xSize, 0);
		glTexCoord2f(0, 1);
		glVertex2d(0, ySize);
		glTexCoord2f(1, 1);
		glVertex2d(xSize, ySize);
		glEnd();
		glPopMatrix();
		glDisable(GL_TEXTURE_2D);
	}

	public static final void drawImage(Texture texture, Color color, double x, double y, double xSize, double ySize)
	{
		glPushMatrix();
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, texture.id);
		glColor4f(color.getRed(), color.getGreen(), color.getBlue(), 1);
		glTranslated(x, y - ySize, 0);
		glBegin(GL_TRIANGLE_STRIP);
		glTexCoord2f(0, 0);
		glVertex2d(0, 0);
		glTexCoord2f(1, 0);
		glVertex2d(xSize, 0);
		glTexCoord2f(0, 1);
		glVertex2d(0, ySize);
		glTexCoord2f(1, 1);
		glVertex2d(xSize, ySize);
		glEnd();
		glPopMatrix();
		glDisable(GL_TEXTURE_2D);
	}

	public static final void drawRectangle(Color color, double x, double y, double xSize, double ySize)
	{
		glPushMatrix();
		glColor4f(color.getRed(), color.getGreen(), color.getBlue(), 1);
		glTranslated(x, y - ySize, 0);
		glBegin(GL_TRIANGLE_STRIP);
		glTexCoord2f(0, 0);
		glVertex2d(0, 0);
		glTexCoord2f(1, 0);
		glVertex2d(xSize, 0);
		glTexCoord2f(0, 1);
		glVertex2d(0, ySize);
		glTexCoord2f(1, 1);
		glVertex2d(xSize, ySize);
		glEnd();
		glPopMatrix();
	}

	public static final void drawText(String text, Color color, int x, int y, int xSize)
	{
		char[] a = text.toLowerCase().toCharArray();
		int xpos = x;
		for (int i = 0; i < a.length; i++)
		{
			Texture texture;
			if (a[i] == '.')
				texture = Texture.loadTexture("lang/period.png");
			else if (a[i] == '/')
				texture = Texture.loadTexture("lang/slash.png");
			else if (a[i] == ':')
				texture = Texture.loadTexture("lang/colon.png");
			else if (a[i] == '\'')
				texture = Texture.loadTexture("lang/apos.png");
			else if (a[i] == '!')
				texture = Texture.loadTexture("lang/excl.png");
			else if (a[i] == '+')
				texture = Texture.loadTexture("lang/plus.png");
			else if (a[i] == ' ')
			{
				xpos += xSize * 2;
				continue;
			} else
			{
				texture = Texture.loadTexture("lang/" + Character.toString(a[i]) + ".png");
			}
			drawImage(texture, color, xpos, y + (texture.height * xSize), texture.width * xSize, texture.height * xSize);
			xpos += (texture.width * xSize) + xSize;
		}
	}
}