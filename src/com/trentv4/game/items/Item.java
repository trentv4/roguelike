package com.trentv4.game.items;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;

import com.trentv4.game.content.GameLoop;
import com.trentv4.game.core.MainGame;
import com.trentv4.game.entity.Entity;

public class Item
{
	public String className;
	public ItemStatistics stats;
	
	private static HashMap<String, Item> ITM_LIST = new HashMap<String, Item>();
	
	//Constructors
	public Item()
	{
		this.className = "Item";
		this.stats = new ItemStatistics();
	}
	
	//Static helper methods
	public static final void populate()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader(new File("items.txt")));
			String next = reader.readLine();
			while(next != null)
			{
				Item a = GameLoop.gson.fromJson(next, Item.class);
				if(!a.className.equals("Item"))
				{
					a = (Item) GameLoop.gson.fromJson(next, Class.forName(a.className));
				}
				ITM_LIST.put(a.stats.ITM_ID, a);
				next = reader.readLine();
			}
			reader.close();
			
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static final Item getNewItem(String lookup)
	{
		try
		{
			if(lookup.equals("RANDOM"))
			{
				Object[] collection = ITM_LIST.values().toArray();
				Item a = (Item) collection[(int) Math.floor(Math.random() * collection.length)];
				if(a != null)
				{
					return a;
				}
			}
			else
			{
				Item a = (Item) ITM_LIST.get(lookup).clone();
				if(a != null)
				{
					return a;
				}
			}
			MainGame.crash(new Exception("Unable to look up item: " + lookup));
			return null;

		} catch (Exception e)
		{
			MainGame.crash(e);
			return null;
		}
	}
	
	//Subclassing methods
	/** Fires every single tick, regardless of which speed tick it is. Only fires while it is equipped. */
	public void onWearTick(Entity owner)
	{
		
	}
	
	/** Fires when an entity picks this item up. NOTE: Use sparingly! */
	public void onPickUp(Entity owner)
	{
		
	}
	
	/** Fires when an entity equips this item successfully. */
	public void onEquip(Entity owner)
	{
		
	}
	
	/** Fires every single tick, regardless of which speed tick it is. */
	public void onTick()
	{
		
	}
}