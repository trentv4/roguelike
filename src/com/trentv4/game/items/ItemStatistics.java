package com.trentv4.game.items;

public class ItemStatistics
{
	public String name;
	public String ITM_ID;
	public int damage;
	public int defense;
	
	public ItemStatistics()
	{
		this.name = "UNKNOWN ITEM!";
		this.ITM_ID = "UNKOWN";
		this.damage = 0;
		this.defense = 0;
	}
	
	public ItemStatistics(String name)
	{
		this.name = name;
	}
}	
