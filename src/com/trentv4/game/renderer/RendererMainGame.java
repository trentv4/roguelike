package com.trentv4.game.renderer;

import com.trentv4.game.content.GameLoop;
import com.trentv4.game.core.DisplayManager;
import com.trentv4.game.logging.Logger;
import com.trentv4.game.terrain.Terrain;

public class RendererMainGame implements Renderer
{
	@Override
	public void render()
	{
		Terrain[][][] t = GameLoop.getMap().terrain;
		for(int x = 0; x < 25; x++)
		{
			for(int y = 0; y < 25; y++)
			{
				for(int z = 0; z < 4; z++)
				{
					DisplayManager.drawImage(t[x][y][z].stats.TER_ID + ".png", 25*x, 25*y, 25, 25);
					Logger.log("Rendering");
				}
			}
		}
	}
}
