package com.trentv4.game.renderer;

public interface Renderer
{
	public void render();
}
