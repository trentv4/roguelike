# Game #

This is a basic top-down old-style roguelike similar to [POWDER](http://www.zincland.com/powder/), an old favorite of mine. 

### How do I get going? ###

* Git clone.
* Add project to Eclipse (.gitignore is only set up for Eclipse).
* Add the LWJGL3 (http://lwjgl.org/) jar and add natives for your system
* Run.


### What? ###

If you can't clone, you don't play until release.


### Controls ###

	TAB: Inventory
	wasd: move in inventory
	e: use (eat)
	enter: select (for moving around)

ingame:

	WASD: move
	F: dig
	L: look
	G: pick up
	spacebar: restart
	to attack, walk into something

### Who do I talk to? ###

* Trent VanSlyke. Found here: trentvanslyke@gmail.com, @trentv4, jamieswhiteshirt.com/trentv4
